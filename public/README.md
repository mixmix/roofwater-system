# Clean Water From A Roof Rainwater System

Rainwater collected from a roof may contain a variety of debris: windblown dust and dirt, plant seeds and leaves, animal droppings, nesting material and eggs, occasionally even rodents or birds.
Provided the installation has a large holding tank or tanks, it is possible to put together a simple low maintenance system that removes almost all of these impurities, and delivers soft potable water. The steps are as follows:

## 1. PREVENT THE LARGER DEBRIS FROM ENTERING THE TANK/S

> Fit a Leaf Filter at each downpipe. 
This is a filter screen, set at an angle so that the water falls through it, while solid debris is caught and deflected out to the front. They are available in a number of designs, but many have the filter too flat. For low maintenance (self-clearing) choose the ones with a very steep filter angle and a filter of vertical bars, rather than mesh or holes. You can also make your own, but be sure the the screen and water catcher are large to avoid losing water.

![](./images/LEAF_FILTER.jpg)

Older systems sometimes had a filter box: a header box with a flat filter mesh. This is a really bad idea, not only because it requires regular cleaning, but because all incoming water is potentially contaminated by running through the accumulated debris.


## 2. DO NOT DROP THE WATER IN THE TOP OF THE TANK

> The water arriving at the tank is clear of large debris, but contains dust, dirt and various dissolved contaminants. Most of this will sink over time, leaving clear water at the top of the tank: dropping the water in the top of the tank re-contaminates all the water in the tank every time it rains.
To avoid this, either bring the rainwater in the bottom of the tank (if the fitting there is large enough) or bring it in the top, but pipe it to the bottom of the tank.

This is an easy retrofit, if necessary, using a bucket to catch the water coming in at the top of the tank, with a downpipe fitted to the bottom of the bucket to a point 400mm off the tank floor. Our setup has a 10L paint pail with a length of downpipe attached. If strong flow is expected from large roof areas, it is also worth considering a simple deflector at the bottom of this pipe: the object is to have fresh rainwater arrive near the bottom of the tank without stirring up the sediment on the bottom.


## 3. DO NOT TAKE WATER FROM THE BOTTOM OF THE TANK

> Over time, all the suspended solids will fall to the bottom of the tank, and the water in the upper tank will be completely clear. In addition, all sorts of complex natural biological activity will be taking place. This is pretty much what happens in lakes and wetlands, and the key element here is time.
To access the clear clean water in the upper tank, fit a floating takeoff. 

![](./images/FLOATING_TAKEOFF.jpg)

These are now available as a flexible pipe with a ballcock float attached. 
The float holds the takeoff point approx 300mm below the water surface. A plastic disc (or cross in this example) keeps the takeoff clear of sediment at the bottom if the tank is emptied.

A simple low cost DIY version of this can be made using a short length of silicone hose, stainless hose clips and a length of plastic electrical conduit with a float attached to the end. Drill multiple large holes in the conduit at the takeoff end to avoid any chance of blockage. 
With preparation, this can be retrofitted to tanks containing water with very little loss. This is what i have.

![](./images/FLOATING_TAKEOFF_DIY.jpg)


## 4. FIT MULTIPLE TANKS IN SERIES, DUPLICATING THE SAME SYSTEM.

> Studies have been carried out (see link below). One large tank set up this way gives excellent results. With two tanks set up in series, the reduction in bacterial activity, in vs out, is exponentially huge.

![](./images/TANKS_SERIAL.jpg)

## 5. OTHER FACTORS.

Use opaque tanks with well fitted lids to avoid the growth of algae.

Tank breathers should allow minimum light in and be fitted with insect mesh.

Do fit isolating valves before and after each tank, before and after pumps etc, to allow future maintenance without draining the system.

Plant round tanks for summer shade and cooling.


## 6. A NOTE ON FIRST FLUSH DIVERTERS.

> This is a valve setup at the top of a cylinder made of large diameter tube. The first water from a rainfall goes to this cylinder, only when it is full is water delivered to the tanks. After rain, the cylinder slowly empties to waste, so provided there are no blockages (?), ongoing operation is automatic.

![](./images/FIRST_FLUSH_DIVERTER.png)

This is a really good idea in principal: the first flush of water from the roof and gutters will contain most of the contaminants.
However there are practical problems unless the roof is quite small:
The container would need to be quite large (much larger than those shown) to effectively flush a larger roof. And since most roofs have multiple downpipes (we collect water from 4) multiple diverters would be required…or one very large one after all the flows come together. But this would need to factor in the water in the pipe network. And so the whole thing rapidly scales up in cost, size and quantity of water dumped.

When I originally put our system in, my one regret was being unable to integrate a first flush system. Since then, I have been relieved to find that it was not necessary. But I would certainly revisit the idea for a small roof/small tank situation.


## 7. GETTING THE WATER TO THE TANK/S.

> For simple installations, where the tank/s are next to the rainwater roofs, the water can often be run directly from the gutters (through a leaf filter) and on down into the tank. 
However where this is not practical, the downpipes may be run under the ground, then back up, the pressure of the water in the house downpipes pushing the water up and into the first tank. This is called a 'wet'system'

![](./images/TANKS_WATER_IN.jpg)

Things to consider when installing a wet system:
During and after rain, water will stand in all the downpipes, up to the height of where the water goes into the tank, so the pipework will be under real and constant pressure: all pipework, from leaf filters down and especially any underground, needs to be durable, completely watertight and well supported.
Over time, silt may build up in the underground pipework: use adequate sized pipe, run any underground network with a very slight fall, and be sure to provide access at the lowest point for future cleaning.
Consider also adding a second large capped access point, just above ground where the pipework makes it's way back up to the tank: opening this dumps the water from all the downpipes and if this happens rapidly you can be confident the network is not badly silted.


## 8. GETTING WATER FROM LOW GUTTERS TO THE TANKS.

We have a workshop with leantos for firewood, and gutters too low to feed into the top of our tanks. To catch this water we first run it into a 200L drum. This is fitted with a cheap sump pump, with integral level switch: when the drum is full, the pump pushes the water up into the tanks. Screendoor mesh stretched and tented across the top of the drum strains out debris and keeps the mosquitos out. 


## 8. LIVED EXPERIENCE.

I have run the above system here in New Zealand for 17years. It is our only source of water for house and garden.

I have a wet system, collecting water from several downpipes into an underground network of 90mm pipes. At this point, the flow through these pipes (as judged by opening and dumping at the tanks) is excellent, with no outward sign of significant silting. The water goes to 2 x 25,000Litre tanks, set up in series.

The water in the tanks is completely clear, with a light coating of very fine silt on the bottom. I have no intention of cleaning the tanks, I believe the biological activity is part of the natural cleaning process.

I have a single small fine gauze filter after the tanks. I open and clean this of fine residue once a year. 

The only other maintenance is regular checking and periodic clearing of gutters and leaf filters, especially after long dry spells when debris can accumulate.

The system produces soft clean water for bathing, cooking and drinking.

I am happy to receive questions and comments at bobirving@gmail.com

## 9.  ATTRIBUTION.

Much of the above derives from work carried out by the ongoing Massey University Roofwater Harvesting Study, headed by Prof Stan Abbott.

https://www.massey.ac.nz/massey/learning/departments/centres-research/roof-water-research-centre/rwrc_home.cfm

